from flask import render_template


def add_routes(application):
    @application.route("/")
    def index():
        return render_template("index.html")

    @application.route("/twow")
    def twow():
        return render_template("twow.html")
