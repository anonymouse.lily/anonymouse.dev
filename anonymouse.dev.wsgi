import py.staticpages as sp
from flask import Flask
application = Flask(__name__)
sp.add_routes(application)

if __name__ == "__main__":
    application.run()
