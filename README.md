# anonymouse.dev

A personal website for myself.

## Plans/ideas:
* Simulator games, similar to BrantSteele
* Portfolio of projects
* Comic archive
* Possible revival of TWOW Online
* Discord bot documentation
* Blog/articles
* Anything else I think of later
